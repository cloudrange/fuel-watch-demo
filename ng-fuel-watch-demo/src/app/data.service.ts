import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { String, StringBuilder } from 'typescript-string-operations';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

 
  constructor(private http: HttpClient) { }

  getFuelStations(product:Number,region:Number,stateRegion:Number,suburb:string,day:boolean,callback: (fuelStationsData: Object)=>void){
    var apiurl = String.Format("https://fuelwatchdemoapi20200712151757.azurewebsites.net/fuelwatch?product={0}", product);

    if(day){
      apiurl = String.Format("{0}&day={1}", apiurl,"tomorrow");
    }
    if(!String.IsNullOrWhiteSpace(suburb)){
      apiurl = String.Format("{0}&suburb={1}", apiurl,suburb);
    }else{
      if(region>0){
        apiurl = String.Format("{0}&region={1}", apiurl,region);
      }else{
        if(stateRegion>0){
        apiurl = String.Format("{0}&stateRegion={1}", apiurl,stateRegion);
        }
      }
    }

    var retVal = this.http.get(apiurl).subscribe(
      data => callback(data),
      error => console.log('oops', error)
    );

    return retVal;
  }
}
