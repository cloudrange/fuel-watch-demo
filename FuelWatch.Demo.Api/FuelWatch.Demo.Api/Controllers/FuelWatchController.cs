﻿using System;
using System.Collections.Generic;
using System.Linq;
using FuelWatch.Demo.Api.Model;
using FuelWatch.Demo.Api.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using static FuelWatch.Demo.Api.Model.FuelWatchViewModel;

namespace FuelWatch.Demo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FuelWatchController : ControllerBase
    {

        private readonly ILogger<FuelWatchController> _logger;
        private IMemoryCache _cache;

        public FuelWatchController(ILogger<FuelWatchController> logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _cache = memoryCache;
            if (FuelWatchService.FuelWatchCache == null)
            {
                FuelWatchService.FuelWatchCache = _cache;
            }
        }

        [HttpGet]
        public IEnumerable<FuelStation> GetFuelStation([FromQuery] int product, [FromQuery] string region, [FromQuery] string stateRegion, [FromQuery] string suburb, [FromQuery] string day)
        {
            var model = new FuelWatchViewModel(product, region, stateRegion, suburb, day);

            return model.FuelStations;

        }

    }
}
