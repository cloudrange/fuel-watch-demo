﻿using FuelWatch.Demo.Api.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FuelWatch.Demo.Api.Model
{

    public class FuelWatchViewModel
    {

        private static FuelWatchService _fuelWatchService = new FuelWatchService(); // ToDo - insted use dependency injection here

        public class FuelStation
        {
            public FuelStation(string title, string content,string lat,string lng,string product)
            {
                Title = title;
                Content = content;
                Lat = lat;
                Lng = lng;
                Product = product;
                Price = double.Parse(title.Split(':')[0]);
            }

            public string Lat { get; set; }

            public string Lng { get; set; }

            public string Title { get; set; }

            public string Content { get; set; }

            public string Product { get; set; }

            public string LatLng { get { return $"{Lat},{Lng}"; } }

            public double Price { get; private set; } 
        }


        public List<FuelStation> FuelStations { get; private set; }

        public FuelWatchViewModel(int product, string region,string stateRegion, string suburb,string day)
        {
            FuelStations = _fuelWatchService.FuelStationsSearch(product, region, stateRegion, suburb, day);
        }
    }
}
