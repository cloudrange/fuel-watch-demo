﻿using SimpleFeedReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static FuelWatch.Demo.Api.Model.FuelWatchViewModel;
using FuelWatch.Demo.Api.Extension;
using FuelWatch.Demo.Api.Model;
using Microsoft.Extensions.Caching.Memory;

namespace FuelWatch.Demo.Api.Service
{
    public class FuelWatchService
    {
        public static IMemoryCache FuelWatchCache { get; set; }

        private const string FuelWatchRssUrl = "http://www.fuelwatch.wa.gov.au/fuelwatch/fuelWatchRSS?Surrounding=yes";

        public List<FuelStation> FuelStationsSearch(int product, string region, string stateRegion, string suburb, string day)
        {
            var retVal = new List<FuelStation>();
            var url = $"{FuelWatchRssUrl}&Product={product}";

            if (!string.IsNullOrWhiteSpace(suburb))
            {
                url = $"{url}&Suburb={suburb}";
            } else
            if (!string.IsNullOrWhiteSpace(region))
            {
                url = $"{url}&Region={region}";
            }
            else 
            if (!string.IsNullOrWhiteSpace(stateRegion))
            {
                url = $"{url}&StateRegion={stateRegion}";
            }

            if (!string.IsNullOrWhiteSpace(day))
            {
                url = $"{url}&Day={day}";
            }

            var rssFeedReader = new FeedReader();
            var fuelWatchItems = rssFeedReader.RetrieveFeed(url);

            string productName = FuelWatchLookUpData.ProductCodes[product];
            foreach (var fuelWatchItem in fuelWatchItems)
            {
                try
                {
                    var infoFields = fuelWatchItem.Content.Replace("Address:", string.Empty).Split(',');
                    var streetAddress = infoFields[0].Trim();
                    var suburbName = infoFields[1].Trim().ToLower().FirstCharToUpper();
                    var fullAddress = $"{streetAddress}, {suburbName} WA";

                    FuelStation cacheItem;
                    FuelStation fuelStation;
                    FuelWatchCache.TryGetValue(fullAddress, out cacheItem);
                    if (cacheItem == null) {
                        var latLngInfo = MapsHelper.GetdtLatLong(fullAddress);
                        var lat = latLngInfo.Rows[0].ItemArray[2].ToString();
                        var lng = latLngInfo.Rows[0].ItemArray[3].ToString();
                        fuelStation = new FuelStation(fuelWatchItem.Title, fuelWatchItem.Content, lat, lng, productName);
                        FuelWatchCache.Set(fullAddress, fuelStation);
                    }
                    else
                    {
                        fuelStation = new FuelStation(fuelWatchItem.Title, fuelWatchItem.Content, cacheItem.Lat, cacheItem.Lng, productName);
                    }

                    retVal.Add(fuelStation);

                }
                catch(Exception ex) { 
                    // ToDo -  skip error handling since it is just for a quick demo 
                }
            }

            return retVal;
        }
    }
}
