﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Net;
using System.Data;

namespace FuelWatch.Demo.Api.Service
{
    public class MapsHelper
    {

        public static DataTable GetdtLatLong(string address)
        {
            var url = $"https://maps.google.com/maps/api/geocode/xml?address={address}&key=<Your Key Here>&sensor=false;";
            //DataTable dtGMap = null;

            WebRequest request = WebRequest.Create(url);

            using (WebResponse response = (HttpWebResponse)request.GetResponse())

            {

                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))

                {

                    DataSet dsResult = new DataSet();

                    dsResult.ReadXml(reader);

                    DataTable dtCoordinates = new DataTable();

                    dtCoordinates.Columns.AddRange(new DataColumn[4] { 
                        new DataColumn("Id", typeof(int)),
                        new DataColumn("Address", typeof(string)),
                        new DataColumn("Latitude",typeof(string)),
                        new DataColumn("Longitude",typeof(string)) 
                    });

                    foreach (DataRow row in dsResult.Tables["result"].Rows)
                    {

                        string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();

                        DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];

                        dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);

                    }

                    return dtCoordinates;
                }
            }
        }
    }
}
