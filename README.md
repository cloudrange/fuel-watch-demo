# Fuel Watch Demo #

This demo give a map view presentation to data in [fuelwatch.wa.gov.au](https://www.fuelwatch.wa.gov.au/fuelwatch/pages/home.jspx).
The implementation for this demo has two main components:

* FuelWatch.Demo.Api - Back End - ASP.Net Core Web API
* ng-fuel-watch-demo - Front End - Angular 7

Both components deployed to Azure as App Services.

Click to see the [FuelWatch map view demo](https://ng-fuelwatch-demo.azurewebsites.net)
### How its work? ###

![Deployment Diagram](https://bitbucket.org/cloudrange/fuel-watch-demo/raw/6adfc28302fb458824de263d78b013828b9026c3/media/images/DeploymentDiagram.png)

### Technologies / Api's ###
* Angular 7
* Asp.Net Core (C#)
* [Angular Google Maps (AGM)](https://angular-maps.com)
* RSS
* Azure
* HTML, CSS

### Configuration ###
* Server side configuration - FuelWatch.Demo.Api allow CORS.

### Performance ###
* Store fuel station location (lat,lng) in [Asp.Net Core Memory cache](https://docs.microsoft.com/en-us/aspnet/core/performance/caching/memory?view=aspnetcore-3.1) to avoid re translating address to LatLng using Google Maps API.

### Summary ###

This demo created in a rush for quick presentation so few development milestones skipped
* No unit tests
* Bugs
* Basic UX

However, the basic functionality in place.
